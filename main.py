from kivy.app import App
from kivy.properties import StringProperty
from kivy.uix.widget import Widget
import os
os.environ['KIVY_VIDEO'] = 'ffpyplayer'
os.environ['KIVY_AUDIO'] = 'sdl2'

class videoplay(Widget):
	address = StringProperty('rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4')
	def __init__(self, **kwargs):
		super(videoplay,self).__init__(**kwargs)
	

	def play_pause(self):
		if self.ids["main_player"].state == "stop":
			self.ids["main_player"].state = "play"
			self.ids["main_button"].text = "Stop"
		else:
			self.ids["main_player"].state = "stop"
			self.ids["main_button"].text = "Play"

class myApp(App):

	def build(self):

		return videoplay()

if __name__ == '__main__':
	myapp=myApp()
	myapp.run()

# from kivy.app import App
# from kivy.uix.video import Video
# from kivy.uix.widget import Widget

# class VideoWindow(App):
# 	def build(self):

# 		video = Video(source='http://192.168.1.97/streams/2/feedback')
# 		video.state = 'play'
# 		video.options = {'eos': 'loop'}
		

# 		video.allow_stretch = True
# 		return video

# if __name__ == "__main__":
# 	window = VideoWindow()
# 	window.run()